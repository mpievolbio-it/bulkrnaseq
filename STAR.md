[Read mapping-based (HISAT2) <<<](HISAT2.html)

**7. Read mapping-based (STAR)**

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
# define samples
S001="KO-BM4"
S002="KO-BM8"
S003="KO-BM9"
S004="WT-BM1"
S005="WT-BM3"
S006="WT-BM7"
```

### Creation of reference genome index

Get reference FASTA and GTF file (skip this step, if you have already donwloaded for other mapping tools before)

```
cd $MYPROJECT/index/reference
wget http://ftp.ensembl.org/pub/release-105/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
wget http://ftp.ensembl.org/pub/release-105/gtf/mus_musculus/Mus_musculus.GRCm39.105.chr.gtf.gz
gunzip Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
gunzip Mus_musculus.GRCm39.105.chr.gtf.gz
samtools faidx Mus_musculus.GRCm39.dna.primary_assembly.fa
```

Create index

```
cd $MYPROJECT/index/reference
STAR \
--runThreadN 12 \
--runMode genomeGenerate \
--genomeDir $MYPROJECT/index/star \
--genomeFastaFiles Mus_musculus.GRCm39.dna.primary_assembly.fa \
--sjdbGTFfile Mus_musculus.GRCm39.105.chr.gtf \
--sjdbOverhang 100
```

### Read mapping against the reference genome

1-Pass mapping

```
cd $MYPROJECT/data
#S001
STAR \
--runThreadN 48 \
--genomeDir $MYPROJECT/index/star \
--readFilesIn $S001"_rRNA_fwd.fq.gz" $S001"_rRNA_rev.fq.gz" \
--readFilesCommand "gunzip -c" \
--outFileNamePrefix $MYPROJECT/results/star/1-pass/$S001 \
--outSAMstrandField intronMotif
#S002
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S002"_rRNA_fwd.fq.gz" $S002"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/1-pass/$S002 --outSAMstrandField intronMotif
#S003
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S003"_rRNA_fwd.fq.gz" $S003"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/1-pass/$S003 --outSAMstrandField intronMotif
#S004
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S004"_rRNA_fwd.fq.gz" $S004"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/1-pass/$S004 --outSAMstrandField intronMotif
#S005
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S005"_rRNA_fwd.fq.gz" $S005"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/1-pass/$S005 --outSAMstrandField intronMotif
#S006
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S006"_rRNA_fwd.fq.gz" $S006"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/1-pass/$S006 --outSAMstrandField intronMotif
```

2-Pass mapping

### Sort and indexing alignments (samtools)

```
cd $MYPROJECT/data
#S001
STAR \
--runThreadN 48 \
--genomeDir $MYPROJECT/index/star \
--readFilesIn $S001"_rRNA_fwd.fq.gz" $S001"_rRNA_rev.fq.gz" \
--readFilesCommand "gunzip -c" \
--outFileNamePrefix $MYPROJECT/results/star/2-pass/$S001 \
--sjdbFileChrStartEnd $MYPROJECT/results/star/1-pass/*.tab \
--outSAMstrandField intronMotif
#S002
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S002"_rRNA_fwd.fq.gz" $S002"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/2-pass/$S002 --sjdbFileChrStartEnd $MYPROJECT/results/star/1-pass/*.tab --outSAMstrandField intronMotif
#S003
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S003"_rRNA_fwd.fq.gz" $S003"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/2-pass/$S003 --sjdbFileChrStartEnd $MYPROJECT/results/star/1-pass/*.tab --outSAMstrandField intronMotif
#S004
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S004"_rRNA_fwd.fq.gz" $S004"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/2-pass/$S004 --sjdbFileChrStartEnd $MYPROJECT/results/star/1-pass/*.tab --outSAMstrandField intronMotif
#S005
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S005"_rRNA_fwd.fq.gz" $S005"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/2-pass/$S005 --sjdbFileChrStartEnd $MYPROJECT/results/star/1-pass/*.tab --outSAMstrandField intronMotif
#S006
STAR --runThreadN 48 --genomeDir $MYPROJECT/index/star --readFilesIn $S006"_rRNA_fwd.fq.gz" $S006"_rRNA_rev.fq.gz" --readFilesCommand "gunzip -c" --outFileNamePrefix $MYPROJECT/results/star/2-pass/$S006 --sjdbFileChrStartEnd $MYPROJECT/results/star/1-pass/*.tab --outSAMstrandField intronMotif
```

### Sort and indexing alignments (samtools)

```
samtools sort -o $MYPROJECT/results/star/2-pass/$S001".Aligned.out.bam" $MYPROJECT/results/star/2-pass/$S001".Aligned.out.sam"
samtools index $MYPROJECT/results/star/2-pass/$S001".Aligned.out.bam"
samtools sort -o $MYPROJECT/results/star/2-pass/$S002".Aligned.out.bam" $MYPROJECT/results/star/2-pass/$S002".Aligned.out.sam"
samtools index $MYPROJECT/results/star/2-pass/$S002".Aligned.out.bam"
samtools sort -o $MYPROJECT/results/star/2-pass/$S003".Aligned.out.bam" $MYPROJECT/results/star/2-pass/$S003".Aligned.out.sam"
samtools index $MYPROJECT/results/star/2-pass/$S003".Aligned.out.bam"
samtools sort -o $MYPROJECT/results/star/2-pass/$S004".Aligned.out.bam" $MYPROJECT/results/star/2-pass/$S004".Aligned.out.sam"
samtools index $MYPROJECT/results/star/2-pass/$S004".Aligned.out.bam"
samtools sort -o $MYPROJECT/results/star/2-pass/$S005".Aligned.out.bam" $MYPROJECT/results/star/2-pass/$S005".Aligned.out.sam"
samtools index $MYPROJECT/results/star/2-pass/$S005".Aligned.out.bam"
samtools sort -o $MYPROJECT/results/star/2-pass/$S006".Aligned.out.bam" $MYPROJECT/results/star/2-pass/$S006".Aligned.out.sam"
samtools index $MYPROJECT/results/star/2-pass/$S006".Aligned.out.bam"
rm $MYPROJECT/results/star/1-pass/*.sam
rm $MYPROJECT/results/star/2-pass/*.sam
```

[Next >>> Transcript assembly (StringTie)](STRINGTIE.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
