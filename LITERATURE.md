## References

Andrews, Simon. "FastQC: a quality control tool for high throughput sequence data. 2010." (2017): W29-33. [https://www.bioinformatics.babraham.ac.uk/projects/fastqc/](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)

Bolger, Anthony M., Marc Lohse, and Bjoern Usadel. "Trimmomatic: a flexible trimmer for Illumina sequence data." Bioinformatics 30, no. 15 (2014): 2114-2120. [https://academic.oup.com/bioinformatics/article/30/15/2114/2390096](https://academic.oup.com/bioinformatics/article/30/15/2114/2390096)

Kopylova, Evguenia, Laurent Noé, and Hélène Touzet. "SortMeRNA: fast and accurate filtering of ribosomal RNAs in metatranscriptomic data." Bioinformatics 28, no. 24 (2012): 3211-3217. [https://academic.oup.com/bioinformatics/article/28/24/3211/246053](https://academic.oup.com/bioinformatics/article/28/24/3211/246053)

Kim, Daehwan, Joseph M. Paggi, Chanhee Park, Christopher Bennett, and Steven L. Salzberg. "Graph-based genome alignment and genotyping with HISAT2 and HISAT-genotype." Nature biotechnology 37, no. 8 (2019): 907-915. [https://www.nature.com/articles/s41587-019-0201-4](https://www.nature.com/articles/s41587-019-0201-4)

Li, Heng, Bob Handsaker, Alec Wysoker, Tim Fennell, Jue Ruan, Nils Homer, Gabor Marth, Goncalo Abecasis, and Richard Durbin. "The sequence alignment/map format and SAMtools." Bioinformatics 25, no. 16 (2009): 2078-2079. [https://academic.oup.com/bioinformatics/article/25/16/2078/204688](https://academic.oup.com/bioinformatics/article/25/16/2078/204688)

Dobin, Alexander, Carrie A. Davis, Felix Schlesinger, Jorg Drenkow, Chris Zaleski, Sonali Jha, Philippe Batut, Mark Chaisson, and Thomas R. Gingeras. "STAR: ultrafast universal RNA-seq aligner." Bioinformatics 29, no. 1 (2013): 15-21. [https://academic.oup.com/bioinformatics/article/29/1/15/272537](https://academic.oup.com/bioinformatics/article/29/1/15/272537)

Pertea, Mihaela, Geo M. Pertea, Corina M. Antonescu, Tsung-Cheng Chang, Joshua T. Mendell, and Steven L. Salzberg. "StringTie enables improved reconstruction of a transcriptome from RNA-seq reads." Nature biotechnology 33, no. 3 (2015): 290-295. [https://www.nature.com/articles/nbt.3122](https://www.nature.com/articles/nbt.3122)

Liao, Yang, Gordon K. Smyth, and Wei Shi. "featureCounts: an efficient general purpose program for assigning sequence reads to genomic features." Bioinformatics 30, no. 7 (2014): 923-930. [https://academic.oup.com/bioinformatics/article/30/7/923/232889](https://academic.oup.com/bioinformatics/article/30/7/923/232889)

Patro, Rob, Geet Duggal, Michael I. Love, Rafael A. Irizarry, and Carl Kingsford. "Salmon provides fast and bias-aware quantification of transcript expression." Nature methods 14, no. 4 (2017): 417-419. [https://www.nature.com/articles/nmeth.4197](https://www.nature.com/articles/nmeth.4197)

Bray, Nicolas L., Harold Pimentel, Páll Melsted, and Lior Pachter. "Near-optimal probabilistic RNA-seq quantification." Nature biotechnology 34, no. 5 (2016): 525-527. [https://www.nature.com/articles/nbt.3519](https://www.nature.com/articles/nbt.3519)

Love, Michael I., Wolfgang Huber, and Simon Anders. "Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2." Genome biology 15, no. 12 (2014): 1-21. [https://genomebiology.biomedcentral.com/articles/10.1186/s13059-014-0550-8](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-014-0550-8)

Robinson, Mark D., Davis J. McCarthy, and Gordon K. Smyth. "edgeR: a Bioconductor package for differential expression analysis of digital gene expression data." Bioinformatics 26, no. 1 (2010): 139-140. [https://academic.oup.com/bioinformatics/article/26/1/139/182458](https://academic.oup.com/bioinformatics/article/26/1/139/182458)

Ritchie, Matthew E., Belinda Phipson, D. I. Wu, Yifang Hu, Charity W. Law, Wei Shi, and Gordon K. Smyth. "limma powers differential expression analyses for RNA-sequencing and microarray studies." Nucleic acids research 43, no. 7 (2015): e47-e47. [https://academic.oup.com/nar/article/43/7/e47/2414268](https://academic.oup.com/nar/article/43/7/e47/2414268)

Soneson, Charlotte, Michael I. Love, and Mark D. Robinson. "Differential analyses for RNA-seq: transcript-level estimates improve gene-level inferences." F1000Research 4 (2015). [https://f1000research.com/articles/4-1521/v2](https://f1000research.com/articles/4-1521/v2)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
