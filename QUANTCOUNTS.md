[Transcript assembly (StringTie) <<<](STRINGTIE.html)

## 9. Quantification (StringTie/featureCounts)

See here for a detailed manual of [StringTie](http://ccb.jhu.edu/software/stringtie/index.shtml?t=manual)

See here for a detailed manual of [featureCounts](http://subread.sourceforge.net/featureCounts.html)

__Note:__ For paired-end data, [featureCounts](http://subread.sourceforge.net/featureCounts.html) can count either each read or count mapped read-pairs as fragments (see Read summarization in the [manual](http://subread.sourceforge.net/SubreadUsersGuide.pdf)).

__Note:__ The same is true for overlapping and multi-mapping reads, see e.g. the description of the manual:

```
6.2.6 Count multi-mapping reads and multi-overlapping reads

A multi-overlapping read is a read that overlaps more than one meta-feature when counting
reads at meta-feature level or overlaps more than one feature when counting reads at feature
level. The decision of whether or not to counting these reads is often determined by the
experiment type. We recommend that reads or fragments overlapping more than one gene
are not counted for RNA-seq experiments, because any single fragment must originate from
only one of the target genes but the identity of the true target gene cannot be confidently
determined. On the other hand, we recommend that multi-overlapping reads or fragments are
counted for ChIP-seq experiments because for example epigenetic modifications inferred from
these reads may regulate the biological functions of all their overlapping genes.
```

In this tutorial, for the [featureCounts](http://subread.sourceforge.net/featureCounts.html) counting, paired-end reads are treated as fragments `-p`, only counted if both mapped `-B` and multi-overlapping `-O` and multi-mapping reads are counted `-M`.

In this tutorial, for the [StringTie](http://ccb.jhu.edu/software/stringtie/index.shtml?t=manual) counting, only the GTF file from the reference is used and not the merged GTF file, which can be considered if new splice-variants are of interest.

### StringTie-HISAT2

```
cd $MYPROJECT/results/hisat2
#S001
mkdir $S001
stringtie \
-p 12 \
-e \
-B \
-o $S001/$S001".hisat2.eB.gtf" \
-A $S001".hisat2.tab" \
-G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf \
$S001".hisat2.bam"
#S002
mkdir $S002
stringtie -p 12 -e -B -o $S002/$S002".hisat2.eB.gtf" -A $S002".hisat2.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S002".hisat2.bam"
#S003
mkdir $S003
stringtie -p 12 -e -B -o $S003/$S003".hisat2.eB.gtf" -A $S003".hisat2.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S003".hisat2.bam"
#S004
mkdir $S004
stringtie -p 12 -e -B -o $S004/$S004".hisat2.eB.gtf" -A $S004".hisat2.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S004".hisat2.bam"
#S005
mkdir $S005
stringtie -p 12 -e -B -o $S005/$S005".hisat2.eB.gtf" -A $S005".hisat2.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S005".hisat2.bam"
#S006
mkdir $S006
stringtie -p 12 -e -B -o $S006/$S006".hisat2.eB.gtf" -A $S006".hisat2.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S006".hisat2.bam"
```

### StringTie-STAR

```
cd $MYPROJECT/results/star/2-pass
#S001
mkdir $S001
stringtie \
-p 12 \
-e \
-B \
-o $S001/$S001".star.eB.gtf" \
-A $S001".star.tab" \
-G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf \
$S001".Aligned.out.bam"
#S002
mkdir $S002
stringtie -p 12 -e -B -o $S002/$S002".star.eB.gtf" -A $S002".star.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S002".Aligned.out.bam"
#S003
mkdir $S003
stringtie -p 12 -e -B -o $S003/$S003".star.eB.gtf" -A $S003".star.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S003".Aligned.out.bam"
#S004
mkdir $S004
stringtie -p 12 -e -B -o $S004/$S004".star.eB.gtf" -A $S004".star.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S004".Aligned.out.bam"
#S005
mkdir $S005
stringtie -p 12 -e -B -o $S005/$S005".star.eB.gtf" -A $S005".star.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S005".Aligned.out.bam"
#S006
mkdir $S006
stringtie -p 12 -e -B -o $S006/$S006".star.eB.gtf" -A $S006".star.tab" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S006".Aligned.out.bam"
```

### FeatureCounts-HISAT2

```

```

### FeatureCounts-STAR

```

```

[Next >>> Read mapping-like (salmon)](SALMON.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
