[Read QC (FastQC) <<<](QC.html)

## 4. Adapter and quality trimming (Trimmomatic)

Depending on the sequence technology, the correct adapter needs to be selected. In this tutorial Illumina sequences are pre-processed.

**Note:** In this tutorial only the retained paired-end reads are further processed, of course it is possible to use the retained single-end reads after filtering. Anyhow, for this inclusion, one would need to map the single-end reads against the reference either directly (HISAT2) or in a seperate step (salmon, kallisto) and merge the resulting mappings and counts afterwards to represent valid fragment counts.

For [trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic), the following Illumina-platform files can be directly selected:

- NexteraPE-PE.fa
- TruSeq2-PE.fa
- TruSeq2-SE.fa
- TruSeq3-PE-2.fa
- TruSeq3-PE.fa
- TruSeq3-SE.fa 

See here for more information about choosing the correct adapter file:

**add link here**

In addition to the adapter sequences, other quality values can be filtered with `trimmomatic`. Please, visit the [manual](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf) to read more about these possible trimming options:

- ILLUMINACLIP: Cut adapter and other illumina-specific sequences from the read.
- SLIDINGWINDOW: Perform a sliding window trimming, cutting once the average quality within the window falls below a threshold.
- LEADING: Cut bases off the start of a read, if below a threshold quality
- TRAILING: Cut bases off the end of a read, if below a threshold quality
- CROP: Cut the read to a specified length
- HEADCROP: Cut the specified number of bases from the start of the read
- MINLEN: Drop the read if it is below a specified length
- AVGQUAL: Drop the read if average quality is below specified threshold
- TOPHRED33: Convert quality scores to Phred-33
- TOPHRED64: Convert quality scores to Phred-64

Of course, there are other tools that are commonly used for this pre-processing step and might even offer more options, just to mention some:

- [cutadapt](https://cutadapt.readthedocs.io/en/stable/)
- [fastp](https://github.com/OpenGene/fastp)
- [prinseq](http://prinseq.sourceforge.net/)

In some situations, e.g. too small insert sizes for paired-end read data, one would like to merge overlapping reads, to not falsely count a gene twice:

- [fastp](https://github.com/OpenGene/fastp)
- [pear](https://www.h-its.org/downloads/pear-academic/)

The same is true for duplicated reads, which can be filtered before mapping:

- [fastp](https://github.com/OpenGene/fastp)
- [prinseq](http://prinseq.sourceforge.net/)

Anyhow, here, [trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) will be used.

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
MYPROJECT="myproject"
cd $MYPROJECT/data
# define samples
S001="KO-BM4"
S001_FWD="KO-BM4_1.fastq.gz"
S001_REV="KO-BM4_2.fastq.gz"
S002="KO-BM8"
S002_FWD="KO-BM8_1.fastq.gz"
S002_REV="KO-BM8_2.fastq.gz"
S003="KO-BM9"
S003_FWD="KO-BM9_1.fastq.gz"
S003_REV="KO-BM9_2.fastq.gz"
S004="WT-BM1"
S004_FWD="WT-BM1_1.fastq.gz"
S004_REV="WT-BM1_2.fastq.gz"
S005="WT-BM3"
S005_FWD="WT-BM3_1.fastq.gz"
S005_REV="WT-BM3_2.fastq.gz"
S006="WT-BM7"
S006_FWD="WT-BM7_1.fastq.gz"
S006_REV="WT-BM7_2.fastq.gz"
```

```
# define adapter file
ADAPTERFILE="$HOME/.conda/envs/bulkrnaseq/share/trimmomatic/adapters/TruSeq3-PE-2.fa"
# trimm PE reads with the following options
# ILLUMINACLIP:$ADAPTERFILE:2:30:10:2
# LEADING:15
# TRAILING:15
# SLIDINGWINDOW:4:15
# AVGQUAL:20
# MINLEN:75
# S001
trimmomatic PE -threads 12 $S001_FWD $S001_REV $S001"_PE1.fq.gz" $S001"_SE1.fq.gz" $S001"_PE2.fq.gz" $S001"_SE2.fq.gz" \
ILLUMINACLIP:$ADAPTERFILE:2:30:10:2 \
LEADING:15 \
TRAILING:15 \
SLIDINGWINDOW:4:15 \
AVGQUAL:20 \
MINLEN:75
#S002
trimmomatic PE -threads 12 $S002_FWD $S002_REV $S002"_PE1.fq.gz" $S002"_SE1.fq.gz" $S002"_PE2.fq.gz" $S002"_SE2.fq.gz" ILLUMINACLIP:$ADAPTERFILE:2:30:10:2 LEADING:15 TRAILING:15 SLIDINGWINDOW:4:15 AVGQUAL:20 MINLEN:75
#S003
trimmomatic PE -threads 12 $S003_FWD $S003_REV $S003"_PE1.fq.gz" $S003"_SE1.fq.gz" $S003"_PE2.fq.gz" $S003"_SE2.fq.gz" ILLUMINACLIP:$ADAPTERFILE:2:30:10:2 LEADING:15 TRAILING:15 SLIDINGWINDOW:4:15 AVGQUAL:20 MINLEN:75
#S004
trimmomatic PE -threads 12 $S004_FWD $S004_REV $S004"_PE1.fq.gz" $S004"_SE1.fq.gz" $S004"_PE2.fq.gz" $S004"_SE2.fq.gz" ILLUMINACLIP:$ADAPTERFILE:2:30:10:2 LEADING:15 TRAILING:15 SLIDINGWINDOW:4:15 AVGQUAL:20 MINLEN:75
#S005
trimmomatic PE -threads 12 $S005_FWD $S005_REV $S005"_PE1.fq.gz" $S005"_SE1.fq.gz" $S005"_PE2.fq.gz" $S005"_SE2.fq.gz" ILLUMINACLIP:$ADAPTERFILE:2:30:10:2 LEADING:15 TRAILING:15 SLIDINGWINDOW:4:15 AVGQUAL:20 MINLEN:75
#S006
trimmomatic PE -threads 12 $S006_FWD $S006_REV $S006"_PE1.fq.gz" $S006"_SE1.fq.gz" $S006"_PE2.fq.gz" $S006"_SE2.fq.gz" ILLUMINACLIP:$ADAPTERFILE:2:30:10:2 LEADING:15 TRAILING:15 SLIDINGWINDOW:4:15 AVGQUAL:20 MINLEN:75
```

[Next >>> Removal of ribosomal-RNA (SortMeRNA)](REMOVERIBO.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
