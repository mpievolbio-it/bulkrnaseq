[Read mapping-based (STAR) <<<](STAR.html)

## 8. Transcript assembly (StringTie)

See here for a detailed manual of [StringTie](http://ccb.jhu.edu/software/stringtie/index.shtml?t=manual)

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
MYPROJECT="myproject"
# define samples
S001="KO-BM4"
S002="KO-BM8"
S003="KO-BM9"
S004="WT-BM1"
S005="WT-BM3"
S006="WT-BM7"
```

### HISAT2

```
cd $MYPROJECT/results/hisat2
#S001
stringtie \
-p 12 \
-o $S001".stringtie.gtf" \
-G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf \
$S001".hisat2.bam"
#S002
stringtie -p 12 -o $S002".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S002".hisat2.bam"
#S003
stringtie -p 12 -o $S003".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S003".hisat2.bam"
#S004
stringtie -p 12 -o $S004".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S004".hisat2.bam"
#S005
stringtie -p 12 -o $S005".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S005".hisat2.bam"
#S006
stringtie -p 12 -o $S006".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S006".hisat2.bam"
```

Transcript merge mode

```
stringtie -p 12 --merge -o hisat2.merged.gtf -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf *.gtf
```

### STAR

```
cd $MYPROJECT/results/star/2-pass
#S001
stringtie \
-p 12 \
-o $S001".stringtie.gtf" \
-G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf \
$S001".Aligned.out.bam"
#S002
stringtie -p 12 -o $S002".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S002".Aligned.out.bam"
#S003
stringtie -p 12 -o $S003".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S003".Aligned.out.bam"
#S004
stringtie -p 12 -o $S004".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S004".Aligned.out.bam"
#S005
stringtie -p 12 -o $S005".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S005".Aligned.out.bam"
#S006
stringtie -p 12 -o $S006".stringtie.gtf" -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf $S006".Aligned.out.bam"
```

Transcript merge mode

```
stringtie -p 12 --merge -o star.merged.gtf -G $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf *.gtf
```


[Next >>> Quantification (StringTie/featureCounts)](QUANTCOUNTS.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
