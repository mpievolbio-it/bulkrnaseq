[Read mapping-like (kallisto) <<<](KALLISTO.html)

## 12. Count table import into R (tximport)

See here for a detailed manual of [tximport](https://bioconductor.org/packages/release/bioc/vignettes/tximport/inst/doc/tximport.html)

In brief, `tximport` import and summarize transcript-level abundance estimates for transcript- and gene-level analysis with Bioconductor packages, such as `edgeR`, `DESeq2`, and `limma-voom`. The motivation and methods for the functions provided by the `tximport` package are described in the following article ([Soneson, Love, and Robinson 2015](http://dx.doi.org/10.12688/f1000research.7563.1)).

Get the working directory of your project

```
MYPROJECT="myproject"
cd $MYPROJECT
pwd
```

Please set the working directory accordingly (need to uncomment the first line and substitute with your folder)

### Load libraries

```
#setwd("/home/ullrich/myproject")
library(readr)
library(dplyr)
library(DESeq2)
library(edgeR)
library(limma)
library(tximport)
library(rhdf5)
```

### Define sample sheet and experimental design

```
samples <- data.frame(matrix(NA, ncol=3, nrow=6))
colnames(samples) <- c("sample", "path", "treatment")
samples$sample <- c("KO-BM4","KO-BM8","KO-BM9","WT-BM1","WT-BM3","WT-BM7")
samples$path <- samples$sample
samples$treatment <- factor(c("KO", "KO", "KO", "WT", "WT", "WT"), levels = c("KO", "WT"))
Treatment <- factor(samples$treatment)
#
design <- model.matrix(~Treatment)
rownames(design) <- Treatment
```

### tximport - StringTie - HISAT2

#### transcript-level

```
files <- file.path("results", "hisat2", samples$path, "t_data.ctab")
names(files) <- samples$sample
txi.hisat2 <- tximport(files, type = "stringtie", txOut=TRUE)
head(txi.hisat2$counts)
head(txi.hisat2$abundance) #TPM
head(txi.hisat2$length) #effective length
names(txi.hisat2)
save(txi.hisat2, file="results/hisat2/txi.hisat2.RData")
```

#### gene-level

```
files <- file.path("results", "hisat2", samples$path, "t_data.ctab")
names(files) <- samples$sample
tmp <- read_tsv(files[1])
tx2gene <- tmp[, c("t_name", "gene_id")]
txi.hisat2.gene <- tximport(files, type = "stringtie", txOut = FALSE, tx2gene = tx2gene)
head(txi.hisat2.gene$counts)
head(txi.hisat2.gene$abundance) #TPM
head(txi.hisat2.gene$length) #effective length
names(txi.hisat2.gene)
save(txi.hisat2.gene, file="results/hisat2/txi.hisat2.gene.RData")
```

### tximport - StringTie - STAR

#### transcript-level

```
files <- file.path("results", "star", "2-pass", samples$path, "t_data.ctab")
names(files) <- samples$sample
txi.star <- tximport(files, type = "stringtie", txOut=TRUE)
head(txi.star$counts)
head(txi.star$abundance) #TPM
head(txi.star$length) #effective length
names(txi.star)
save(txi.star, file="results/star/txi.star.RData")
```

#### gene-level

```
files <- file.path("results", "star", samples$path, "t_data.ctab")
names(files) <- samples$sample
tmp <- read_tsv(files[1])
tx2gene <- tmp[, c("t_name", "gene_id")]
txi.star.gene <- tximport(files, type = "stringtie", txOut = FALSE, tx2gene = tx2gene)
head(txi.star.gene$counts)
head(txi.star.gene$abundance) #TPM
head(txi.star.gene$length) #effective length
names(txi.star.gene)
save(txi.star.gene, file="results/star/txi.star.gene.RData")
```

### tximport - salmon

#### transcript-level

```
files <- file.path("results", "salmon", samples$path, "quant.sf")
names(files) <- samples$sample
txi.salmon <- tximport(files, type = "salmon", txOut = TRUE)
head(txi.salmon$counts)
head(txi.salmon$abundance) #TPM
head(txi.salmon$length) #effective length
names(txi.salmon)
save(txi.salmon, file="results/salmon/txi.salmon.RData")
```

#### gene-level

```
t2g <- read_tsv("index/salmon/Mus_musculus.GRCm39.105.chr.gtf.t2g.txt", col_names = NULL)
colnames(t2g) <- c("GENEID", "TXNAME", "GENESHORT", "GENETYPE", "PROTEINID")
tx2gene <- dplyr::select(t2g, "TXNAME", "GENEID")
files <- file.path("results", "salmon", samples$path, "quant.sf")
names(files) <- samples$sample
txi.salmon.gene <- tximport(files, type = "salmon", txOut = FALSE, tx2gene = tx2gene)
head(txi.salmon.gene$counts)
head(txi.salmon.gene$abundance) #TPM
head(txi.salmon.gene$length) #effective length
names(txi.salmon.gene)
save(txi.salmon.gene, file="results/salmon/txi.salmon.gene.RData")
```

### tximport - kallisto

#### transcript-level

```
files <- file.path("results", "kallisto", samples$path, "abundance.h5")
names(files) <- samples$sample
txi.kallisto <- tximport(files, type = "kallisto", txOut = TRUE)
head(txi.kallisto$counts)
head(txi.kallisto$abundance) #TPM
head(txi.kallisto$length) #effective length
names(txi.kallisto)
save(txi.kallisto, file="results/kallisto/txi.kallisto.RData")
```

#### gene-level

```
t2g <- read_tsv("index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.t2g.txt", col_names = NULL)
colnames(t2g) <- c("GENEID", "TXNAME", "GENESHORT", "GENETYPE", "PROTEINID")
tx2gene <- dplyr::select(t2g, "TXNAME", "GENEID")
files <- file.path("results", "kallisto", samples$path, "abundance.h5")
names(files) <- samples$sample
txi.kallisto.gene <- tximport(files, type = "kallisto", txOut = FALSE, tx2gene = tx2gene)
head(txi.kallisto.gene$counts)
head(txi.kallisto.gene$abundance) #TPM
head(txi.kallisto.gene$length) #effective length
names(txi.kallisto.gene)
save(txi.kallisto.gene, file="results/kallisto/txi.kallisto.gene.RData")
```

[Next >>> Detection of DEGs (DESeq2)](DESEQ2.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
