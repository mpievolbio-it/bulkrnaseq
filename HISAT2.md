[Removal of ribosomal-RNA (SortMeRNA) <<<](REMOVERIBO.html)

## 6. Read mapping-based (HISAT2)

See here for a detailed manual of [HISAT2](http://daehwankimlab.github.io/hisat2/manual/)

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
MYPROJECT="myproject"
# define samples
S001="KO-BM4"
S002="KO-BM8"
S003="KO-BM9"
S004="WT-BM1"
S005="WT-BM3"
S006="WT-BM7"
```

### Creation of reference genome index

Get reference FASTA and GTF file

```
cd $MYPROJECT/index/reference
wget http://ftp.ensembl.org/pub/release-105/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
wget http://ftp.ensembl.org/pub/release-105/gtf/mus_musculus/Mus_musculus.GRCm39.105.chr.gtf.gz
gunzip Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
gunzip Mus_musculus.GRCm39.105.chr.gtf.gz
samtools faidx Mus_musculus.GRCm39.dna.primary_assembly.fa
```

Create splice site list

```
cd $MYPROJECT/index/hisat2
cat $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf | hisat2_extract_splice_sites.py - > Mus_musculus.GRCm39.105.chr.gtf.splice_sites
```

Create exon list

```
cat $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf | hisat2_extract_exons.py - > Mus_musculus.GRCm39.105.chr.gtf.exons
```

Create index

```
hisat2-build -p 12 --ss Mus_musculus.GRCm39.105.chr.gtf.splice_sites \
--exon Mus_musculus.GRCm39.105.chr.gtf.exons \
$MYPROJECT/index/reference/Mus_musculus.GRCm39.dna.primary_assembly.fa \
Mus_musculus.GRCm39.dna.primary_assembly
```

### Read mapping against the reference genome

Map reads against the reference

```
cd $MYPROJECT/data
#S001
hisat2 -p 48 --very-sensitive --dta \
--summary-file $MYPROJECT/results/hisat2/$S001".hisat2.summary" \
-x $MYPROJECT/index/hisat2/Mus_musculus.GRCm39.dna.primary_assembly \
-1 $S001"_rRNA_fwd.fq.gz" \
-2 $S001"_rRNA_rev.fq.gz" \
-S $MYPROJECT/results/hisat2/$S001".hisat2.sam"
#S002
hisat2 -p 48 --very-sensitive --dta --summary-file $MYPROJECT/results/hisat2/$S002".hisat2.summary" -x $MYPROJECT/index/hisat2/Mus_musculus.GRCm39.dna.primary_assembly -1 $S002"_rRNA_fwd.fq.gz" -2 $S002"_rRNA_rev.fq.gz" -S $MYPROJECT/results/hisat2/$S002".hisat2.sam"
#S003
hisat2 -p 48 --very-sensitive --dta --summary-file $MYPROJECT/results/hisat2/$S003".hisat2.summary" -x $MYPROJECT/index/hisat2/Mus_musculus.GRCm39.dna.primary_assembly -1 $S003"_rRNA_fwd.fq.gz" -2 $S003"_rRNA_rev.fq.gz" -S $MYPROJECT/results/hisat2/$S003".hisat2.sam"
#S004
hisat2 -p 48 --very-sensitive --dta --summary-file $MYPROJECT/results/hisat2/$S004".hisat2.summary" -x $MYPROJECT/index/hisat2/Mus_musculus.GRCm39.dna.primary_assembly -1 $S004"_rRNA_fwd.fq.gz" -2 $S004"_rRNA_rev.fq.gz" -S $MYPROJECT/results/hisat2/$S004".hisat2.sam"
#S005
hisat2 -p 48 --very-sensitive --dta --summary-file $MYPROJECT/results/hisat2/$S005".hisat2.summary" -x $MYPROJECT/index/hisat2/Mus_musculus.GRCm39.dna.primary_assembly -1 $S005"_rRNA_fwd.fq.gz" -2 $S005"_rRNA_rev.fq.gz" -S $MYPROJECT/results/hisat2/$S005".hisat2.sam"
#S006
hisat2 -p 48 --very-sensitive --dta --summary-file $MYPROJECT/results/hisat2/$S006".hisat2.summary" -x $MYPROJECT/index/hisat2/Mus_musculus.GRCm39.dna.primary_assembly -1 $S006"_rRNA_fwd.fq.gz" -2 $S006"_rRNA_rev.fq.gz" -S $MYPROJECT/results/hisat2/$S006".hisat2.sam"
```

### Sort and indexing alignments (samtools)

```
samtools sort -o $MYPROJECT/results/hisat2/$S001".hisat2.bam" $MYPROJECT/results/hisat2/$S001".hisat2.sam"
samtools index $MYPROJECT/results/hisat2/$S001".hisat2.bam"
samtools sort -o $MYPROJECT/results/hisat2/$S002".hisat2.bam" $MYPROJECT/results/hisat2/$S002".hisat2.sam"
samtools index $MYPROJECT/results/hisat2/$S002".hisat2.bam"
samtools sort -o $MYPROJECT/results/hisat2/$S003".hisat2.bam" $MYPROJECT/results/hisat2/$S003".hisat2.sam"
samtools index $MYPROJECT/results/hisat2/$S003".hisat2.bam"
samtools sort -o $MYPROJECT/results/hisat2/$S004".hisat2.bam" $MYPROJECT/results/hisat2/$S004".hisat2.sam"
samtools index $MYPROJECT/results/hisat2/$S004".hisat2.bam"
samtools sort -o $MYPROJECT/results/hisat2/$S005".hisat2.bam" $MYPROJECT/results/hisat2/$S005".hisat2.sam"
samtools index $MYPROJECT/results/hisat2/$S005".hisat2.bam"
samtools sort -o $MYPROJECT/results/hisat2/$S006".hisat2.bam" $MYPROJECT/results/hisat2/$S006".hisat2.sam"
samtools index $MYPROJECT/results/hisat2/$S006".hisat2.bam"
rm $MYPROJECT/results/hisat2/*.sam
```

[Next >>> Read mapping-based (STAR)](STAR.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
