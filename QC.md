[Input data <<<](INPUTDATA.html)

## 3. Read QC (FastQC)

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) aims to provide a simple way to do some quality control checks on raw sequence data coming from high throughput sequencing pipelines.

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
```

```
MYPROJECT="myproject"
cd $MYPROJECT/data
# define samples
S001="KO-BM4"
S001_FWD="KO-BM4_1.fastq.gz"
S001_REV="KO-BM4_2.fastq.gz"
S002="KO-BM8"
S002_FWD="KO-BM8_1.fastq.gz"
S002_REV="KO-BM8_2.fastq.gz"
S003="KO-BM9"
S003_FWD="KO-BM9_1.fastq.gz"
S003_REV="KO-BM9_2.fastq.gz"
S004="WT-BM1"
S004_FWD="WT-BM1_1.fastq.gz"
S004_REV="WT-BM1_2.fastq.gz"
S005="WT-BM3"
S005_FWD="WT-BM3_1.fastq.gz"
S005_REV="WT-BM3_2.fastq.gz"
S006="WT-BM7"
S006_FWD="WT-BM7_1.fastq.gz"
S006_REV="WT-BM7_2.fastq.gz"
# QC
fastqc --nogroup -o . -t 12 --kmers 7 $S001_FWD $S001_REV
fastqc --nogroup -o . -t 12 --kmers 7 $S002_FWD $S002_REV
fastqc --nogroup -o . -t 12 --kmers 7 $S003_FWD $S003_REV
fastqc --nogroup -o . -t 12 --kmers 7 $S004_FWD $S004_REV
fastqc --nogroup -o . -t 12 --kmers 7 $S005_FWD $S005_REV
fastqc --nogroup -o . -t 12 --kmers 7 $S006_FWD $S006_REV
```

Now, you can inspect the corresponding FastQC report files to look e.g. for over-represented adpater sequences.

[Next >>> Adapter and quality trimming (Trimmomatic)](TRIMMING.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
