[Quantification (StringTie/featureCounts) <<<](QUANTCOUNTS.html)

## 10. Read mapping-like (salmon)

See here for a detailed manual of [salmon](https://salmon.readthedocs.io/en/latest/salmon.html)

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
MYPROJECT="myproject"
# define samples
S001="KO-BM4"
S002="KO-BM8"
S003="KO-BM9"
S004="WT-BM1"
S005="WT-BM3"
S006="WT-BM7"
```

### Creation of decoy-aware transcriptome index

Get reference FASTA and GTF file (skip this step, if you have already donwloaded for other mapping tools before)

```
cd $MYPROJECT/index/reference
wget http://ftp.ensembl.org/pub/release-105/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
wget http://ftp.ensembl.org/pub/release-105/gtf/mus_musculus/Mus_musculus.GRCm39.105.chr.gtf.gz
gunzip Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
gunzip Mus_musculus.GRCm39.105.chr.gtf.gz
samtools faidx Mus_musculus.GRCm39.dna.primary_assembly.fa
```

Create transcripts from GTF

See here for more information about the [gffread](https://ccb.jhu.edu/software/stringtie/gff.shtml) tool to extract transcripts from a genome

and see here for a in-depth explanation, why it is a difference to call read counts either against genes or isoforms [IsoformSwitchAnalyzeR](https://bioconductor.org/packages/release/bioc/vignettes/IsoformSwitchAnalyzeR/inst/doc/IsoformSwitchAnalyzeR.html)

```
gffread -w Mus_musculus.GRCm39.105.chr.gtf.transcripts.fa \
-g $MYPROJECT/index/reference/Mus_musculus.GRCm39.dna.primary_assembly.fa \
$MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf
python $MYPROJECT/scripts/t2gGTF.py -i $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -o Mus_musculus.GRCm39.105.chr.gtf.t2g.txt -g -b -p -s
```

Create full decoy

See here for more information about [full decoy](https://combine-lab.github.io/alevin-tutorial/2019/selective-alignment/)

```
grep "^>" $MYPROJECT/index/reference/Mus_musculus.GRCm39.dna.primary_assembly.fa | cut -d " " -f 1 > decoys.txt
sed -i.bak -e 's/>//g' decoys.txt
cat Mus_musculus.GRCm39.105.chr.gtf.transcripts.fa $MYPROJECT/index/reference/Mus_musculus.GRCm39.dna.primary_assembly.fa > gentrome.fa
```

Create index

```
conda activate salmon
salmon index \
-p 12 \
-t gentrome.fa \
-i Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon \
--decoys decoys.txt \
-k 31
```

### Read quantification

Map reads against the reference

```
#S001
salmon quant \
-p 12 \
--numBootstraps 100 \
--gcBias \
-i $MYPROJECT/index/salmon/Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon \
-g $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf \
-l IU \
-1 $S001"_rRNA_fwd.fq.gz" \
-2 $S001"_rRNA_rev.fq.gz" \
--validateMappings \
-o $MYPROJECT/results/salmon/$S001
#S002
salmon quant -p 12 --numBootstraps 100 --gcBias -i $MYPROJECT/index/salmon/Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon -g $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -l IU -1 $S002"_rRNA_fwd.fq.gz" -2 $S002"_rRNA_rev.fq.gz" --validateMappings -o $MYPROJECT/results/salmon/$S002
#S003
salmon quant -p 12 --numBootstraps 100 --gcBias -i $MYPROJECT/index/salmon/Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon -g $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -l IU -1 $S003"_rRNA_fwd.fq.gz" -2 $S003"_rRNA_rev.fq.gz" --validateMappings -o $MYPROJECT/results/salmon/$S003
#S004
salmon quant -p 12 --numBootstraps 100 --gcBias -i $MYPROJECT/index/salmon/Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon -g $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -l IU -1 $S004"_rRNA_fwd.fq.gz" -2 $S004"_rRNA_rev.fq.gz" --validateMappings -o $MYPROJECT/results/salmon/$S004
#S005
salmon quant -p 12 --numBootstraps 100 --gcBias -i $MYPROJECT/index/salmon/Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon -g $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -l IU -1 $S005"_rRNA_fwd.fq.gz" -2 $S005"_rRNA_rev.fq.gz" --validateMappings -o $MYPROJECT/results/salmon/$S005
#S006
salmon quant -p 12 --numBootstraps 100 --gcBias -i $MYPROJECT/index/salmon/Mus_musculus.GRCm39.105.chr.gtf.transcripts.salmon -g $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -l IU -1 $S006"_rRNA_fwd.fq.gz" -2 $S006"_rRNA_rev.fq.gz" --validateMappings -o $MYPROJECT/results/salmon/$S006
```

[Next >>> Read mapping-like (kallisto)](KALLISTO.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
