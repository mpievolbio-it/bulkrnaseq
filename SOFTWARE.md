## Software tools

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) Andrews, Simon. "FastQC: a quality control tool for high throughput sequence data. 2010." (2017): W29-33.

[Trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) Bolger, Anthony M., Marc Lohse, and Bjoern Usadel. "Trimmomatic: a flexible trimmer for Illumina sequence data." Bioinformatics 30, no. 15 (2014): 2114-2120.

[SortMeRNA](https://github.com/biocore/sortmerna) Kopylova, Evguenia, Laurent Noé, and Hélène Touzet. "SortMeRNA: fast and accurate filtering of ribosomal RNAs in metatranscriptomic data." Bioinformatics 28, no. 24 (2012): 3211-3217.

[HISAT2](http://daehwankimlab.github.io/hisat2/main/) Kim, Daehwan, Joseph M. Paggi, Chanhee Park, Christopher Bennett, and Steven L. Salzberg. "Graph-based genome alignment and genotyping with HISAT2 and HISAT-genotype." Nature biotechnology 37, no. 8 (2019): 907-915.

[samtools](https://github.com/samtools/samtools) Li, Heng, Bob Handsaker, Alec Wysoker, Tim Fennell, Jue Ruan, Nils Homer, Gabor Marth, Goncalo Abecasis, and Richard Durbin. "The sequence alignment/map format and SAMtools." Bioinformatics 25, no. 16 (2009): 2078-2079.

[STAR](https://github.com/alexdobin/STAR) Dobin, Alexander, Carrie A. Davis, Felix Schlesinger, Jorg Drenkow, Chris Zaleski, Sonali Jha, Philippe Batut, Mark Chaisson, and Thomas R. Gingeras. "STAR: ultrafast universal RNA-seq aligner." Bioinformatics 29, no. 1 (2013): 15-21.

[StringTie](https://ccb.jhu.edu/software/stringtie/) Pertea, Mihaela, Geo M. Pertea, Corina M. Antonescu, Tsung-Cheng Chang, Joshua T. Mendell, and Steven L. Salzberg. "StringTie enables improved reconstruction of a transcriptome from RNA-seq reads." Nature biotechnology 33, no. 3 (2015): 290-295.

[featureCounts](http://subread.sourceforge.net/) Liao, Yang, Gordon K. Smyth, and Wei Shi. "featureCounts: an efficient general purpose program for assigning sequence reads to genomic features." Bioinformatics 30, no. 7 (2014): 923-930.

[salmon](https://github.com/COMBINE-lab/salmon) Patro, Rob, Geet Duggal, Michael I. Love, Rafael A. Irizarry, and Carl Kingsford. "Salmon provides fast and bias-aware quantification of transcript expression." Nature methods 14, no. 4 (2017): 417-419.

[kallisto](https://pachterlab.github.io/kallisto) Bray, Nicolas L., Harold Pimentel, Páll Melsted, and Lior Pachter. "Near-optimal probabilistic RNA-seq quantification." Nature biotechnology 34, no. 5 (2016): 525-527.

[R](https://cran.r-project.org/)

[tximport](https://bioconductor.org/packages/release/bioc/html/tximport.html) Soneson, Charlotte, Michael I. Love, and Mark D. Robinson. "Differential analyses for RNA-seq: transcript-level estimates improve gene-level inferences." F1000Research 4 (2015).

[DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html) Love, Michael I., Wolfgang Huber, and Simon Anders. "Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2." Genome biology 15, no. 12 (2014): 1-21.

[edgeR](https://bioconductor.org/packages/release/bioc/html/edgeR.html) Robinson, Mark D., Davis J. McCarthy, and Gordon K. Smyth. "edgeR: a Bioconductor package for differential expression analysis of digital gene expression data." Bioinformatics 26, no. 1 (2010): 139-140.

[limma](https://bioconductor.org/packages/release/bioc/html/limma.html) Ritchie, Matthew E., Belinda Phipson, D. I. Wu, Yifang Hu, Charity W. Law, Wei Shi, and Gordon K. Smyth. "limma powers differential expression analyses for RNA-sequencing and microarray studies." Nucleic acids research 43, no. 7 (2015): e47-e47.

[Next >>> References](LITERATURE.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
