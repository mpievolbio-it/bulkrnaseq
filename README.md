## bulk-RNAseq analysis to detect DEGs

The source code of this tutorial can be obtained here:

[https://gitlab.gwdg.de/mpievolbio-it/bulkrnaseq](https://gitlab.gwdg.de/mpievolbio-it/bulkrnaseq)

The webpage of this book is hosted here:

[https://mpievolbio-it.pages.gwdg.de/bulkrnaseq/](https://mpievolbio-it.pages.gwdg.de/bulkrnaseq/)

This tutorial is a basic Illumina-based bulk-RNAseq analysis, which compares a control versus a treatment sample.

## Introduction

To be able to calculate differential expressed genes (DEGs), first, a count table needs to be obtained by mapping or quantifying the RNA-seq reads against a a reference genome. For the mapping step, splice-aware aligners like e.g. HISAT2 or STAR are commonly used. The resulting BAM files are further processed to quantify the annotated genes/transcripts of the reference with e.g. subread-featureCounts. Another approach uses a mapping-like "alignment-free" strategy to directly qunatify a pre-defined set of transcripts of a reference with the software tools kallisto or salmon.

After repeating the quantification of various samples/timepoints, the read counts are imported in R (with e.g. tximport-package). The combined read-count tables are analysed to find DEGs with e.g. DESeq2, edgeR, limma-voom.

__Note:__ What to do if you have no replicates?

Please see the note of the R edgeR-package, what to do if you have no biological replicates.

[https://bioconductor.org/packages/release/bioc/vignettes/edgeR/inst/doc/edgeRUsersGuide.pdf](https://bioconductor.org/packages/release/bioc/vignettes/edgeR/inst/doc/edgeRUsersGuide.pdf)

__Note:__  The R DESeq2-package can not be used to analyze a dataset without replicates!

[https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html](https://bioconductor.org/packages/release/bioc/vignettes/DESeq2/inst/doc/DESeq2.html)


[Next >>> Content](CONTENT.html)

----

These notes have been prepared by Kristian K Ullrich. The copyright for the material in these notes resides with the author.

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
