[Content <<<](CONTENT.html)

## 1. Installation of the software prerequisites with conda

To run this tutorial a [conda](https://www.anaconda.com/) environment will be created. Please install either [anaconda](https://www.anaconda.com/products/individual) or [miniconda](https://docs.conda.io/en/latest/miniconda.html) for this purpuse.

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda create -n bulkrnaseq python=3
conda activate bulkrnaseq
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
```

Install software tools for the `bulkrnaseq` environment.

```
conda install fastqc
conda install trimmomatic
conda install sortmerna
conda install hisat2
conda install samtools
conda install star
conda install stringtie
conda install gffread
conda install subread
conda install kallisto
```

Since sometimes not all software tools can be installed into the same conda environment due to some library clashes, here a dedicated `salmon` environment is created to overcome the following issue [https://github.com/COMBINE-lab/salmon/issues/565](https://github.com/COMBINE-lab/salmon/issues/565). It might be that at time of reading this the issue is resolved and all software tools can go just in the `bulkrnaseq` environment.

```
conda deactivate
conda create -n salmon python=3
conda activate salmon
conda install salmon
conda deactivate
conda activate bulkrnaseq
```

Create a folder for your project.

```
# define project path
MYPROJECT="myproject"
mkdir $MYPROJECT
cd $MYPROJECT
```

Create a folder structure within your project folder.

```
mkdir data
mkdir scripts
mkdir index
mkdir index/sortmerna
mkdir index/reference
mkdir index/hisat2
mkdir index/star
mkdir index/salmon
mkdir index/kallisto
mkdir results
mkdir results/hisat2
mkdir results/star
mkdir results/star/1-pass
mkdir results/star/2-pass
mkdir results/salmon
mkdir results/kallisto
```

Download processing scripts.

```
cd $MYPROJCT/scripts/
wget https://github.com/kullrich/bio-scripts/raw/master/gtf_gff3/t2gGTF.py
```

[Next >>>Input data (SRA/ENA)](INPUTDATA.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
