[Installation of the software prerequisites with conda <<<](CONDAENV.html)

## 2. Input data (SRA/ENA)

For this tutorial a public data set will be used from [Bae J et al. (2019)](https://www.nature.com/articles/s41467-019-11386-4), which bioproject informtaion [PRJNA528590](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA528590) can be used to get some details about the library.

```
Instrument: Illumina HiSeq 4000
Strategy: RNA-Seq
Source: TRANSCRIPTOMIC
Selection: cDNA
Layout: PAIRED
Construction protocol: Bone marrow cells were isolated from the femurs and tibias of 8-week-old mice.
Total RNA was isolated using TRIzol reagent according to the manufacturer's instructions. In order to
construct cDNA libraries with the TruSeq RNA library kit, 1ug of total RNA was used. The protocol
consisted of polyA-selected RNA extraction, RNA fragmentation, random hexamer primed reverse transcription
and 100nt paired-end sequencing by Illumina HiSeq4000
```

The sequencing read data can be either obtained from the ncbi sequencing read archive ([SRA](https://www.ncbi.nlm.nih.gov/sra?linkname=bioproject_sra_all&from_uid=528590)) or from the european nucleotide archive ([ENA](https://www.ebi.ac.uk/ena/browser/view/PRJNA528590?show=reads)).

Each entry is associated with a biological sample ID, which will again give some more information.

E.g. [https://www.ebi.ac.uk/ena/browser/view/SAMN11231439](https://www.ebi.ac.uk/ena/browser/view/SAMN11231439)

```
Accession: SAMN11231439
Sample Title: KO-BM4
Sample Alias: GSM3683312
Source Name: Phc2 knockout_bone marrow
Strain Background: C57BL/6
Age: 8-week-old
Genotype/variation: Phc2+/- (Phc2 heterozygote)
Tissue: Bone marrow
```

```
MYPROJECT="myproject"
cd $MYPROJECT/data
wget -O KO-BM4_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/005/SRR8768135/SRR8768135_1.fastq.gz
wget -O KO-BM4_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/005/SRR8768135/SRR8768135_2.fastq.gz
wget -O KO-BM8_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/006/SRR8768136/SRR8768136_1.fastq.gz
wget -O KO-BM8_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/006/SRR8768136/SRR8768136_2.fastq.gz
wget -O KO-BM9_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/007/SRR8768137/SRR8768137_1.fastq.gz
wget -O KO-BM9_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/007/SRR8768137/SRR8768137_2.fastq.gz
wget -O WT-BM1_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/008/SRR8768138/SRR8768138_1.fastq.gz
wget -O WT-BM1_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/008/SRR8768138/SRR8768138_2.fastq.gz
wget -O WT-BM3_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/009/SRR8768139/SRR8768139_1.fastq.gz
wget -O WT-BM3_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/009/SRR8768139/SRR8768139_2.fastq.gz
wget -O WT-BM7_1.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/000/SRR8768140/SRR8768140_1.fastq.gz
wget -O WT-BM7_2.fastq.gz ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR876/000/SRR8768140/SRR8768140_2.fastq.gz
```

[Next >>> Read QC (FastQC)](QC.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
