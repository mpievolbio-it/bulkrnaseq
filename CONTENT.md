[Introduction <<<](https://mpievolbio-it.pages.gwdg.de/bulkrnaseq/)

## Content

The steps that this tutorial covers are:

1. [Installation of the software prerequisites with conda](CONDAENV.html)
2. [Input data (SRA/ENA)](INPUTDATA.html)
3. [Read QC (FastQC)](QC.html)
4. [Adapter and quality trimming (Trimmomatic)](TRIMMING.html)
5. [Removal of ribosomal-RNA (SortMeRNA)](REMOVERIBO.html)
6. [Read mapping-based (HISAT2)](HISAT2.html#6-read-mapping-based-hisat2)
    1. [Creation of reference genome index](HISAT2.html#creation-of-reference-genome-index)
    2. [Read mapping against the reference genome](HISAT2.html#read-mapping-against-the-reference-genome)
    3. [Sort and indexing alignments (samtools)](HISAT2.html#sort-and-indexing-alignments-samtools)
7. [Read mapping-based (STAR)](STAR.html#7-read-mapping-based-star)
    1. [Creation of reference genome index](STAR.html#creation-of-reference-genome-index)
    2. [Read mapping against the reference genome](STAR.html#read-mapping-against-the-reference-genome)
    3. [Sort and indexing alignments (samtools)](STAR.html#sort-and-indexing-alignments-samtools)
8. [Transcript assembly (StringTie)](STRINGTIE.html#8-transcript-assembly-stringtie)
    1. [HISAT2](STRINGTIE.html#hisat2)
    2. [STAR](STRINGTIE.html#star)
9. [Quantification (StringTie/featureCounts)](QUANTCOUNTS.html#9-quantification-stringtie-featurecounts)
    1. [StringTie-HISAT2](QUANTCOUNTS.html#stringtie-hisat2)
    2. [StringTie-STAR](QUANTCOUNTS.html#stringtie-star)
    3. [FeatureCounts-HISAT2](QUANTCOUNTS.html#featirecounts-hisat2)
    4. [FeatureCounts-STAR](QUANTCOUNTS.html#featurecounts-star)
10. [Read mapping-like (salmon)](SALMON.html#10-read-mapping-like-salmon)
    1. [Creation of decoy-aware transcriptome index](SALMON.html#creation-of-decoy-aware-transcriptome-index)
    2. [Read quantification](SALMON.html#read-quantification)
11. [Read mapping-like (kallisto)](KALLISTO.html#11-read-mapping-like-kallisto)
    1. [Creation of transcriptome index](KALLISTO.html#creation-of-transcriptome-index)
    2. [Read quantification](KALLISTO.html#read-quantification)
12. [Count table import into R (tximport)](TXIMPORT.html)
13. [Detection of DEGs (DESeq2)](DESEQ2.html)
14. [Detection of DEGs (edgeR)](EDGER.html)
15. [Detection of DEGs (limma-voom)](LIMMA.html)
16. [Software Tools](SOFTWARE.html)
17. [References](LITERATURE.html)


[Next >>> Installation of the software prerequisites with conda](CONDAENV.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
