[Adapter and quality trimming (Trimmomatic) <<<](TRIMMING.html)

## 5. Removal of ribosomal-RNA (SortMeRNA)

**Note:** In this tutorial only the retained paired-end reads are further processed, of course it is possible to use the retained single-end reads after filtering. Anyhow, for this inclusion, one would need to map the single-end reads against the reference either directly (HISAT2) or in a seperate step (salmon, kallisto) and merge the resulting mappings and counts afterwards to represent valid fragment counts.

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
MYPROJECT="myproject"
cd $MYPROJECT/data
# define samples
S001="KO-BM4"
S002="KO-BM8"
S003="KO-BM9"
S004="WT-BM1"
S005="WT-BM3"
S006="WT-BM7"
# create directories
mkdir $S001
mkdir $S002
mkdir $S003
mkdir $S004
mkdir $S005
mkdir $S006
```

Get rRNA database files.

```
cd $MYPROJECT/index/sortmerna
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/rfam-5.8s-database-id98.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/rfam-5s-database-id98.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/silva-arc-16s-id95.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/silva-arc-23s-id98.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/silva-bac-16s-id90.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/silva-bac-23s-id98.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/silva-euk-18s-id95.fasta
wget https://github.com/biocore/sortmerna/raw/master/data/rRNA_databases/silva-euk-28s-id98.fasta
for file in *.fasta;do cat $file >> rRNA_database.fasta;done
```


```
cd $MYPROJECT/data
# S001
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S001"_PE1.fq.gz" --reads $S001"_PE2.fq.gz" \
--workdir $S001 \
--other $S001"_rRNA" \
--out2 \
--paired_in \
--fastx \
--threads 48
# S002
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S002"_PE1.fq.gz" --reads $S002"_PE2.fq.gz" --workdir $S002 --other $S002"_rRNA" --out2 --paired_in --fastx --threads 48
# S003
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S003"_PE1.fq.gz" --reads $S003"_PE2.fq.gz" --workdir $S003 --other $S003"_rRNA" --out2 --paired_in --fastx --threads 48
# S004
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S004"_PE1.fq.gz" --reads $S004"_PE2.fq.gz" --workdir $S004 --other $S004"_rRNA" --out2 --paired_in --fastx --threads 48
# S005
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S005"_PE1.fq.gz" --reads $S005"_PE2.fq.gz" --workdir $S005 --other $S005"_rRNA" --out2 --paired_in --fastx --threads 48
# S006
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S006"_PE1.fq.gz" --reads $S006"_PE2.fq.gz" --workdir $S006 --other $S006"_rRNA" --out2 --paired_in --fastx --threads 48
```

**Note:** Only use the following commands if you plan to include retained single-end reads.

```
mkdir $S001"_SE1"
mkdir $S001"_SE2"
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S001"_SE1.fq.gz"\
--workdir $S001"_SE1" \
--other $S001"_SE1_rRNA" \
--out2 \
--fastx \
--threads 48
sortmerna --ref $MYPROJECT/index/sortmerna/rRNA_database.fasta --reads $S001"_SE2.fq.gz"\
--workdir $S001"_SE2" \
--other $S001"_SE2_rRNA" \
--out2 \
--fastx \
--threads 48
```

[Next >>> Read mapping-based (HISAT2)](HISAT2.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
