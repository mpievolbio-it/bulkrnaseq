[Read mapping-like (salmon) <<<](SALMON.html)

## 11. Read mapping-like (kallisto)

See here for a detailed manual of [kallisto](http://pachterlab.github.io/kallisto/manual.html)

If you have not activate the conda environment, please do:

```
source /data/modules/python/python-anaconda3/etc/profile.d/conda.sh
conda activate bulkrnaseq
MYPROJECT="myproject"
# define samples
S001="KO-BM4"
S002="KO-BM8"
S003="KO-BM9"
S004="WT-BM1"
S005="WT-BM3"
S006="WT-BM7"
```

### Creation of transcriptome index

Get reference FASTA and GTF file (skip this step, if you have already donwloaded for other mapping tools before)

```
cd $MYPROJECT/index/reference
wget http://ftp.ensembl.org/pub/release-105/fasta/mus_musculus/dna/Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
wget http://ftp.ensembl.org/pub/release-105/gtf/mus_musculus/Mus_musculus.GRCm39.105.chr.gtf.gz
gunzip Mus_musculus.GRCm39.dna.primary_assembly.fa.gz
gunzip Mus_musculus.GRCm39.105.chr.gtf.gz
samtools faidx Mus_musculus.GRCm39.dna.primary_assembly.fa
```

Create transcripts from GTF

See here for more information about the [gffread](https://ccb.jhu.edu/software/stringtie/gff.shtml) tool to extract transcripts from a genome

and see here for a in-depth explanation, why it is a difference to call read counts either against genes or isoforms [IsoformSwitchAnalyzeR](https://bioconductor.org/packages/release/bioc/vignettes/IsoformSwitchAnalyzeR/inst/doc/IsoformSwitchAnalyzeR.html)

```
gffread -w Mus_musculus.GRCm39.105.chr.gtf.transcripts.fa \
-g $MYPROJECT/index/reference/Mus_musculus.GRCm39.dna.primary_assembly.fa \
$MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf
python $MYPROJECT/scripts/t2gGTF.py -i $MYPROJECT/index/reference/Mus_musculus.GRCm39.105.chr.gtf -o Mus_musculus.GRCm39.105.chr.gtf.t2g.txt -g -b -p -s
```

Create index

```
kallisto index -i Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto Mus_musculus.GRCm39.105.chr.gtf.transcripts.fa
```

### Read quantification

Map reads against the reference

```
cd $MYPROJECT/data
#S001
kallisto quant \
-i $MYPROJECT/index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto \
-o $MYPROJECT/results/kallisto/$S001 \
-b 100 \
-t 48 \
$S001"_rRNA_fwd.fq.gz" \
$S001"_rRNA_rev.fq.gz"
#S002
kallisto quant -i $MYPROJECT/index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto -o $MYPROJECT/results/kallisto/$S002 -b 100 -t 48 $S002"_rRNA_fwd.fq.gz" $S002"_rRNA_rev.fq.gz"
#S003
kallisto quant -i $MYPROJECT/index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto -o $MYPROJECT/results/kallisto/$S003 -b 100 -t 48 $S003"_rRNA_fwd.fq.gz" $S003"_rRNA_rev.fq.gz"
#S004
kallisto quant -i $MYPROJECT/index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto -o $MYPROJECT/results/kallisto/$S004 -b 100 -t 48 $S004"_rRNA_fwd.fq.gz" $S004"_rRNA_rev.fq.gz"
#S005
kallisto quant -i $MYPROJECT/index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto -o $MYPROJECT/results/kallisto/$S005 -b 100 -t 48 $S005"_rRNA_fwd.fq.gz" $S005"_rRNA_rev.fq.gz"
#S006
kallisto quant -i $MYPROJECT/index/kallisto/Mus_musculus.GRCm39.105.chr.gtf.transcripts.kallisto -o $MYPROJECT/results/kallisto/$S006 -b 100 -t 48 $S006"_rRNA_fwd.fq.gz" $S006"_rRNA_rev.fq.gz"
```

[Next >>> Count table import into R (tximport)](TXIMPORT.html)

----

&copy; Kristian K Ullrich (2022) - ullrich@evolbio.mpg.de
